// Code for the main app page (Regions List).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

function viewRegion(regionIndex)
{
    // Save the desired region to local storage so it can be accessed from view region page.
    localStorage.setItem(APP_PREFIX + "-selectedRegion", regionIndex); 
    // ... and load the view region page.
    location.href = 'viewRegion.html';
}
var regions;

var existingRegions = JSON.parse(localStorage.getItem(APP_PREFIX));
if (existingRegions == null) {
  regions = [];
} else {
  regions = JSON.parse(localStorage.getItem(APP_PREFIX))._regions;
}

var listItmes = document.getElementById("regionsList");
regions.map(function (region, index) {
  const li = document.createElement("li");
  li.setAttribute("class", "mdl-list__item mdl-list__item--two-line");

  li.innerHTML = `
   
    <li class="mdl-list__item mdl-list__item--two-line"  >
            <i class="material-icons mdl-list__item-icon" style="margin-right:10px;">location_on</i>
                <span class="mdl-list__item-primary-content">
                
                  <span> ${region._name}</span>
                  <span class="mdl-list__item-sub-title">Created On:  ${region._date}</span>
                </span>
                <button class="mdl-button mdl-js-button mdl-button--mini-fab mdl-button--raised" style="margin-left:20px;" onclick="viewRegion(${index});">
                <i class="material-icons mdl-list__item-icon" >keyboard_arrow_right</i>
              </button>
              </li>
    <li class="li-ship-detail">
        
  `;
  listItmes.appendChild(li);
});
// console.log(regions);
