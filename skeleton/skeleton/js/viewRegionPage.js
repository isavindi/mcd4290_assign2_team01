// Code for the View Region page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion"); 
if (regionIndex !== null)
{
    // If a region index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the region being displayed.
     var regionNames = JSON.parse(localStorage.getItem(APP_PREFIX))._regions;

  if (regionNames == null) {
    regions = [];
  } else {
    regions = JSON.parse(localStorage.getItem(APP_PREFIX))._regions;
  }
  // console.log(regionNames);
  document.getElementById("headerBarTitle").textContent =
    regionNames[regionIndex]._name;
}

let regionTemp = new Region(regions[regionIndex]._name);
regionTemp._cornerLocations = regions[regionIndex]._cornerLocations;
var region = regionTemp._cornerLocations;

var posts = regionTemp.getBoundaryPosts();

mapboxgl.accessToken =
  "pk.eyJ1IjoiY2hhbWE3Nzc3IiwiYSI6ImNraG9sc3JtbjEyNG4zMm14NHFybnAzamUifQ.AAl70fOO_8wqOz0gkSHE9A";
var map = new mapboxgl.Map({
  container: "map", // container id
  style: "mapbox://styles/mapbox/streets-v11",
  center: region[0], // starting position
  zoom: 16, // starting zoom
});
// Finding the mouse location

var geojson = {
  type: "FeatureCollection",
  features: [],
};
var markers = {
  type: "FeatureCollection",
  features: [],
};

// Used to draw a line between points
var linestring = {
  type: "Feature",
  geometry: {
    type: "LineString",
    coordinates: [],
  },
};

// Interacting with the map
map.on("load", function () {
  map.addSource("geojson", {
    type: "geojson",
    data: geojson,
  });
  map.addSource("markers", {
    type: "geojson",
    data: markers,
  });
  // Add styles to the map corners
  map.addLayer({
    id: "measure-points",
    type: "circle",
    source: "geojson",
    paint: {
      "circle-radius": 5,
      "circle-color": "orange",
    },
    filter: ["in", "$type", "Point"],
  });
  // Add styles to the map posts
  map.addLayer({
    id: "post-points",
    type: "circle",
    source: "markers",
    layout: {
      // make layer visible by default
      visibility: "visible",
    },
    paint: {
      "circle-radius": 2,
      "circle-color": "red",
    },
    filter: ["in", "$type", "Point"],
  });
  map.addLayer({
    id: "measure-lines",
    type: "line",
    source: "geojson",
    layout: {
      "line-cap": "round",
      "line-join": "round",
    },
    paint: {
      "line-color": "#000",
      "line-width": 1,
    },
    filter: ["in", "$type", "LineString"],
  });
  var deleteBtn = document.getElementById("btn-delete");

  deleteBtn.addEventListener("mouseup", function () {
    console.log("deleting");
    var updatedRegions = JSON.parse(localStorage.getItem(APP_PREFIX));
    console.log(updatedRegions);
    updatedRegions._regions.splice(regionIndex, 1);
    localStorage.setItem(APP_PREFIX, JSON.stringify(updatedRegions));
    displayMessage("Region has been deleted");
    window.location.replace("/index.html");
  });

  var link = document.createElement("a");
  link.href = "#";
  link.className = "active";
  link.textContent = "post-points";

  link.onclick = function (e) {
    var clickedLayer = this.textContent;
    e.preventDefault();
    e.stopPropagation();

    var visibility = map.getLayoutProperty(clickedLayer, "visibility");

    // toggle layer visibility by changing the layout object's visibility property
    if (visibility === "visible") {
      map.setLayoutProperty(clickedLayer, "visibility", "none");
      this.className = "";
    } else {
      this.className = "active";
      map.setLayoutProperty(clickedLayer, "visibility", "visible");
    }
  };

  var layers = document.getElementById("menu");
  layers.appendChild(link);

  region.forEach(function (el) {
    var point = {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: el,
      },
    };
    geojson.features.push(point);
  });

  posts.forEach(function (el) {
    var point = {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: el,
      },
    };
    markers.features.push(point);
  });

<<<<<<< Updated upstream
 

=======
  var sub = [];
  geojson.features.slice(0, -1).forEach((e) => {
    sub.push(e.geometry.coordinates);
  });

  const ar = document.getElementById("details");
  const { area, perimeter } = regionTemp.findArea();
    
    console.log ("area = " + area);
       console.log ("perimeter = " + perimeter);
  ar.innerHTML = `
  <li class="mdl-list__item mdl-list__item--two-line"  >
      <span class="mdl-list__item-primary-content">
      

        <span class="mdl-list__item-sub-title">Area:  ${area} m<sup>2</sup></span>
      </span>
  </li>
  <li class="mdl-list__item mdl-list__item--two-line"  >
      <span class="mdl-list__item-primary-content">
      
      
        <span class="mdl-list__item-sub-title">Perimeter:  ${perimeter} m</span>
      </span>
  </li>

  `;

  console.log(geojson.features);
  if (geojson.features.length > 1) {
    linestring.geometry.coordinates = geojson.features.map(function (point) {
      return point.geometry.coordinates;
    });

    geojson.features.push(linestring);
  }

  map.getSource("geojson").setData(geojson);
  map.getSource("markers").setData(markers);
});
>>>>>>> Stashed changes
