// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
var savedRegions = [];

//Feature 01 - create region class
class Region {
  constructor(name) {
    this._cornerLocations = [];
    this._date = Date().toString();
    this._name = name;
  }


//Feature 02 - create region list class
class RegionList {
  constructor() {
    this._regions = [];
  }

  get getRegions() {
    return this._regions;
  }

  get numberOfRegions() {
    return this._regions.length();
  }

  set addRegion(region) {
    this._regions.push(region);
  }

  set removeRegion(region) {
    this._regions = this._regions.filter((r) => r !== region);
  }
}
