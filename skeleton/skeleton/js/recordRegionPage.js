var firstClick = true;
mapboxgl.accessToken =
  "pk.eyJ1IjoiY2hhbWE3Nzc3IiwiYSI6ImNraG9sc3JtbjEyNG4zMm14NHFybnAzamUifQ.AAl70fOO_8wqOz0gkSHE9A";
var map = new mapboxgl.Map({
  container: "map", // container id
  style: "mapbox://styles/mapbox/streets-v11",
  center: [79.82983783665156, 8.028130596883997], // starting position
  zoom: 16, // starting zoom
});

<<<<<<< Updated upstream
// Finding the mouse location
=======
// Finding the mouse location - from the reference given
>>>>>>> Stashed changes
map.on("mousemove", function (e) {
  document.getElementById("info").innerHTML =
    // e.point is the x, y coordinates of the mousemove event relative
    // to the top-left corner of the map
    JSON.stringify(e.point) +
    "<br />" +
    // e.lngLat is the longitude, latitude geographical position of the event
    JSON.stringify(e.lngLat.wrap());
});
<<<<<<< Updated upstream

var geojson = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "LineString",
        coordinates: [],
      },
    },
  ],
};

// Used to draw a line between points
var linestring = {
  type: "Feature",
  geometry: {
    type: "LineString",
    coordinates: [],
  },
};
// Interacting with the map
map.on("load", function () {
  map.addSource("geojson", {
    type: "geojson",
    data: geojson,
  });

  // Adding styles to the map
  map.addLayer({
    id: "measure-points",
    type: "circle",
    source: "geojson",
    paint: {
      "circle-radius": 5,
      "circle-color": "orange",
    },
    filter: ["in", "$type", "Point"],
  });
  map.addLayer({
    id: "measure-lines",
    type: "line",
    source: "geojson",
    layout: {
      "line-cap": "round",
      "line-join": "round",
    },
    paint: {
      "line-color": "#000",
      "line-width": 2.5,
    },
    filter: ["in", "$type", "LineString"],
  });
  var resetBtn = document.getElementById("btn-reset");
  var saveBtn = document.getElementById("btn-save");

  //mouseup event - resetting the region when the left mouse button is released over the reset button.
 resetBtn.addEventListener("mouseup", function () {
    console.log("resetting");

   // Adding a new layer
    geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: "LineString",
            coordinates: [],  //Intializing the empty array
          },
        },
      ],
    };
    map.getSource("geojson").setData(geojson);  //Setting the new layer(geojson) as the new source to the map.
    firstClick = true;
  });
  
  //onclick event - saving the region when the save button is clicked.
saveBtn.addEventListener("click", function () {
    var name = document.getElementById("name").value.trim(); //Getting the "name" value from HTML and removing whitespaces from both ends. 
    console.log(name);

    //Creating a new array to store the new points.
    var sub = [];
    geojson.features.slice(0, -1).forEach((e) => {
      sub.push(e.geometry.coordinates);
    });

    //validation for inputing a name to the region.
    if (name === "") {
      displayMessage("You must add a name");  
    } else {
      let region = new Region(name);
      if (sub.length > 3) {
        region._cornerLocations = sub;
        console.log(region._cornerLocations);
        var existingRegions = JSON.parse(localStorage.getItem(APP_PREFIX));
        if (existingRegions == null) existingRegions = new RegionList();
        existingRegions._regions.push(region);
        localStorage.setItem(APP_PREFIX, JSON.stringify(existingRegions));
        window.location.replace("/index.html");
      } else {
        displayMessage("You must add atleast 3 corners before adding");  //validation for inputing a name to the region.
      }
    }
  });
<<<<<<< Updated upstream

map.on("click", function (e) {
    var features = map.queryRenderedFeatures(e.point, {
      layers: ["measure-points"],
    });

    // Remove the linestring from the group
    // So we can redraw it based on the points collection
    if (geojson.features.length > 1) geojson.features.pop();
    // Clear the Distance container to populate it with a new value
    // distanceContainer.innerHTML = '';

    if (firstClick) {
      geojson = {
        type: "FeatureCollection",
        features: [
          {
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: [e.lngLat.lng, e.lngLat.lat],
            },
            properties: {
              id: "start",
            },
          },
          {
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: [e.lngLat.lng, e.lngLat.lat],
            },
            properties: {
              id: "end",
            },
          },
        ],
      };

      firstClick = false;
    } else {
      var point = {
        type: "Feature",
        geometry: {
          type: "Point",
          coordinates: [e.lngLat.lng, e.lngLat.lat],
        },
        properties: {
          id: String(new Date().getTime()),
        },
      };
      geojson.features.splice(geojson.features.length - 1, 0, point);
    }
    console.log(geojson.features);
    if (geojson.features.length > 1) {
      linestring.geometry.coordinates = geojson.features.map(function (point) {
        return point.geometry.coordinates;
      });

      geojson.features.push(linestring);
    }
   
    map.getSource("geojson").setData(geojson);
  });
});
=======
=======
<<<<<<< Updated upstream
=======
>>>>>>> Stashed changes
>>>>>>> Stashed changes
